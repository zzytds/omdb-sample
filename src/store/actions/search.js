import axios from 'axios';
import { addList } from './movies';

const prefix = 'SEARCH_';

export const actionTypes = {
    SEARCH_REQ: prefix + 'SEARCH_REQ',
    SEARCH_RES: prefix + 'SEARCH_RES',
    SEARCH_ERR: prefix + 'SEARCH_ERR'
}

const searchReq = searchText => ({
    type: actionTypes.SEARCH_REQ,
    payload: searchText,
    loading: {
        search: true
    },
    error: {
        search: null
    }
});

const searchRes = response => ({
    type: actionTypes.SEARCH_RES,
    payload: response,
    loading: {
        search: false
    }
});

const searchErr = error => ({
    type: actionTypes.SEARCH_ERR,
    payload: error,
    loading: {
        search: false
    },
    error: {
        search: error
    }
});

export const search = searchText => async (dispatch) => {
    dispatch(searchReq(searchText));
    try {
        const response = await axios.get('http://www.omdbapi.com/', {
            params: {
                apiKey: '6eacaa5d',
                s: searchText,
                type: 'movie'
            }
        });
        if(response.data.Response === 'True') {
            dispatch(addList(response.data.Search));
            dispatch(searchRes(response.data));
        } else {
            throw response.data.Error
        }
    } catch(err) {
        dispatch(searchErr(err));
    }
    
}
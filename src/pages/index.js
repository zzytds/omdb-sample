import SearchPage from './SearchPage';
import WatchListPage from './WatchListPage';

export const appTitle = 'OMdb';

export const pagesList = [
    {
        label: 'Search',
        component: SearchPage,
        path: '/'
    },
    {
        label: 'Watch List',
        component: WatchListPage,
        path: '/watchlist'
    }
]
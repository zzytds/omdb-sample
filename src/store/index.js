import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { reducer as searchReducer } from './reducers/search';
import { reducer as moviesReducer } from './reducers/movies';
import { reducer as watchlistReducer } from './reducers/watchlist';
import { loadingReducer, errorReducer } from './reducers/utilities';

const reducer = combineReducers({
    search: searchReducer,
    movies: moviesReducer,
    watchlist: watchlistReducer,
    loading: loadingReducer,
    errors: errorReducer
});

export default createStore(reducer, composeWithDevTools(
    applyMiddleware(thunk)
));
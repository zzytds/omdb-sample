export const loadingReducer = (state = {}, action) => ({ ...state, ...action.loading});
export const errorReducer = (state = {}, action) => ({ ...state, ...action.error });
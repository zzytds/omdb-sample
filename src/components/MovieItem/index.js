import React, { Component } from 'react';
import {
    ListGroupItem,
    Button
} from 'reactstrap';
import './style.scss';

export default class MovieItem extends Component {
    render() {
        return (
            <ListGroupItem>
                <div className='movie-item-inner'>
                    <div className='movie-item-text'>
                        {this.props.movie.Title} ({this.props.movie.Year})
                    </div>
                    <div className='movie-item-button'>
                    {
                        this.props.movie.inWatchList ?
                        <Button color='success' onClick={() => this.props.onRemove(this.props.movie.imdbID)}>Added</Button> :
                        <Button color='secondary' onClick={() => this.props.onAdd(this.props.movie.imdbID)}>Add to Watchlist</Button>
                    }
                    </div>
                </div>
            </ListGroupItem>
        );
    }
}
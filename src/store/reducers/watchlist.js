import { actionTypes } from '../actions/watchlist';
import { union, difference } from 'lodash';

const initialState = {
    ids: []
}

export const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.ADD: {
            return {
                ids: union(state.ids, [ action.payload ])
            };
        }
        case actionTypes.REMOVE: {
            return {
                ids: difference(state.ids, [ action.payload ])
            }
        }
        default: {
            return state;
        }
    }
}
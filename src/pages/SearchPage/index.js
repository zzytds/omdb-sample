import React, { Component, Fragment } from 'react';
import SearchBar from 'components/SearchBar';
import {
    Row,
    Col
} from 'reactstrap';
import { add, remove } from 'store/actions/watchlist';
import { search } from 'store/actions/search';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import MovieList from 'components/MovieList';
import UtilWrapper from 'components/UtilWrapper';

class SearchPage extends Component {
    render() {
        return (
            <Fragment>
                <Row style={{ marginTop: '50px'}}>
                    <SearchBar onSearch={this.props.search} />
                </Row>
                <Row style={{ marginTop: '50px' }}>
                    <Col xs='12'>
                        <UtilWrapper utilKey='search'>
                            {
                                this.props.searchTerm ? <h1>Results for "{this.props.searchTerm}"</h1> : ''
                            }
                            <MovieList movies={this.props.searchResults} addToList={this.props.addToWatchlist} removeFromList={this.props.removeFromWatchlist}/>
                        </UtilWrapper>
                    </Col>
                </Row>
            </Fragment>
        );
    }
}

const s2p = state => ({
    searchResults: state.search.results.map(id => ({
        ...state.movies.entities[id],
        inWatchList: state.watchlist.ids.includes(id)
    })),
    searchTerm: state.search.searchTerm
});

const d2p = dispatch => ({
    search: searchText => dispatch(search(searchText)),
    addToWatchlist: id => dispatch(add(id)),
    removeFromWatchlist: id => dispatch(remove(id))
});

export default withRouter(connect(s2p, d2p)(SearchPage))
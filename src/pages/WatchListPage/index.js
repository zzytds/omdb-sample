import React, { Component, Fragment } from 'react';
import {
    Row,
    Col
} from 'reactstrap';
import { remove } from 'store/actions/watchlist';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import MovieList from 'components/MovieList';

class WatchListPage extends Component {
    render() {
        return (
            <Fragment>
                <Row>
                    <h1>Your Watchlist</h1>
                </Row>
                <Row>
                    <Col xs='12'>
                        <MovieList movies={this.props.watchlist} removeFromList={this.props.removeFromWatchlist} />
                    </Col>
                </Row>
            </Fragment>
        );
    }
}

const s2p = state => ({
    watchlist: state.watchlist.ids.map(id => ({
        ...state.movies.entities[id],
        inWatchList: true
    }))
});

const d2p = dispatch => ({
    removeFromWatchlist: id => dispatch(remove(id))
});

export default withRouter(connect(s2p, d2p)(WatchListPage));
import { actionTypes } from 'store/actions/movies';
import { normalize, schema } from 'normalizr';
import { difference, union } from 'lodash';

const movieSchema = new schema.Entity('movies', {}, {
    idAttribute: 'imdbID'
});

const initialState = {
    entities: {},
    ids: []
}

export const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.ADD_LIST: {
            const normalized = normalize(action.payload, [ movieSchema ]);
            const newIds = difference(normalized.result, state.ids);
            const newEntities = newIds.reduce((prev,id) => ({...prev, [id]: normalized.entities.movies[id]}), {});
            return {
                ids: union(state.ids, newIds),
                entities: { ...state.entities, ...newEntities }
            }
        }
        case actionTypes.ADD_DETAIL: {
            const id = action.payload.imdbID;
            return {
                ids: union(state.ids, [id]),
                entities: { ...state.entities, [id]: action.payload }
            }
        }
        default: {
            return state;
        }
    }
}
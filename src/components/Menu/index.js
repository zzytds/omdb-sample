import React, { Component } from 'react';
import {
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Collapse,
    Container
} from 'reactstrap';
import { NavLink as RouterLink } from 'react-router-dom';

export default class Menu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    render() {
        return (
            <Navbar color="light" light expand="md">
                <Container>
                    <NavbarBrand to="/" tag={RouterLink}>{this.props.brand}</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            { this.props.items.map(item => (
                                <NavItem key={item.path}>
                                    <NavLink to={item.path} tag={RouterLink} exact={true}>{item.label}</NavLink>
                                </NavItem>
                            ))}
                        </Nav>
                    </Collapse>
                </Container>
            </Navbar>
        )
    }
}
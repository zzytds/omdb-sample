import React, { Component } from 'react';
import {
    InputGroup,
    Input,
    InputGroupAddon,
    Button
} from 'reactstrap'

export default class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state={
            searchText: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    handleChange(e) {
        this.setState({
            searchText: e.target.value
        });
    }

    handleSearch() {
        this.props.onSearch(this.state.searchText);
        this.setState({
            searchText: ''
        })
    }
    
    render() {
        return (
        <InputGroup>
            <Input onChange={this.handleChange} value={this.state.searchText} />
            <InputGroupAddon addonType="append">
                <Button color='primary' disabled={!this.state.searchText} onClick={this.handleSearch}>Search</Button>
            </InputGroupAddon>
        </InputGroup>
    )}
}
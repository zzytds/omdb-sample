import React, { Component } from 'react';
import {
    ListGroup,
    Alert
} from 'reactstrap';
import MovieItem from '../MovieItem';

export default class MovieList extends Component {
    render() {
        return (
            this.props.movies && this.props.movies.length ?
            <ListGroup>
                {
                    this.props.movies.map(item => (
                        <MovieItem key={item.imdbID} movie={item} onAdd={this.props.addToList} onRemove={this.props.removeFromList} />
                    ))
                }
            </ListGroup> :
            <Alert color='info'>
                Nothing to show!
            </Alert>
        );
    }
}
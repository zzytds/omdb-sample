import React, { Component, Fragment } from 'react';
import { appTitle, pagesList } from 'pages';
import {
    Container
} from 'reactstrap';
import { Switch, Route } from 'react-router-dom';
import Menu from 'components/Menu';

export default class App extends Component {
    render() {
        return (
            <Fragment>
                <Menu brand={appTitle} items={pagesList} />
                <Container>
                    <Switch>
                        {
                            pagesList.map(item => (
                                <Route key={item.path} path={item.path} component={item.component} exact={true} />
                            ))
                        }
                    </Switch>
                </Container>
            </Fragment>
        );
    }
}

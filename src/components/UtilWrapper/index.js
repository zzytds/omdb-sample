import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
    Alert
} from 'reactstrap';

class UtilWrapper extends Component {
    componentWillMount() {
        this.props.clearUtil();
    }

    render() {
        return this.props.loading || this.props.error ?
        (    
            <Fragment>
                { this.props.loading ? <Alert>Loading</Alert>: '' }
                { this.props.error ? <Alert color='danger'>Error</Alert>: '' }
            </Fragment>
        ) : this.props.children
    }
}

const s2p = (state, props) => ({
    loading: state.loading[props.utilKey] || false,
    error: state.errors[props.utilKey]
})

const d2p = (dispatch, props) => ({
    clearUtil: () => {
        dispatch({
            type: 'dummy',
            loading: {
                [props.utilKey]: false
            },
            error: {
                [props.utilKey]: null
            }
        });
    }
});

export default withRouter(connect(s2p, d2p)(UtilWrapper));
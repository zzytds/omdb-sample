import axios from 'axios';
import addDetail from './movies';

const prefix = 'DETAILS_';

export const actionTypes = {
    LOAD_REQ: prefix + 'LOAD_REQ',
    LOAD_RES: prefix + 'LOAD_RES',
    LOAD_ERR: prefix + 'LOAD_ERR'
}

const loadReq = id => ({
    type: actionTypes.LOAD_REQ,
    payload: id,
    loading: {
        ['DETAIL_' + id]: true
    },
    error: {
        ['DETAIL_' + id]: null
    }
});

const loadRes = response => ({
    type: actionTypes.LOAD_RES,
    payload: response,
    loading: {
        ['DETAIL_' + id]: true
    },
    error: {
        ['DETAIL_' + id]: null
    }
});

const loadErr = error => ({
    type: actionTypes.LOAD_ERR,
    payload: error,
    loading: {
        ['DETAIL_' + id]: true
    },
    error: {
        ['DETAIL_' + id]: error
    }
});

export const load = id => async (dispatch) => {
    dispatch(loadReq(id));
    try {
        const response = axios.get('http://www.omdbapi.com/', {
            params: {
                i: id,
                apiKey: '6eacaa5d'
            }
        });
        dispatch(loadRes(id));
        dispatch(addDetail(response));
    } catch(error) {
        dispatch(loadErr(error));
    }
}
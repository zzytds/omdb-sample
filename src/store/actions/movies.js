const prefix = 'MOVIES_'

export const actionTypes = {
    ADD_LIST: prefix + 'ADD_LIST',
    ADD_DETAIL: prefix + 'ADD_DETAIL'
}

export const addList = list => ({
    type: actionTypes.ADD_LIST,
    payload: list
})

export const addDetail = detail => ({
    type: actionTypes.ADD_DETAIL,
    payload: detail
})
const prefix = 'WATCHLIST_'

export const actionTypes = {
    ADD: prefix + 'ADD',
    REMOVE: prefix + 'REMOVE'
}

export const add = id => ({
    type: actionTypes.ADD,
    payload: id
});

export const remove = id => ({
    type: actionTypes.REMOVE,
    payload: id
});
import { actionTypes } from '../actions/search';

const initialState = {
    results: [],
    pageNum: null,
    total: null,
    searchTerm: null
}

export const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.SEARCH_REQ: {
            return {
                ...initialState,
                searchTerm: action.payload
            }
        }
        case actionTypes.SEARCH_RES: {
            return {
                ...state,
                results: action.payload.Search.map(item => item.imdbID),
                pageNum: 1,
                total: action.payload.totalResults
            }
        }
        default: {
            return state;
        }
    }
}